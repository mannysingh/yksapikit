//
//  YKSError.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/16/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSError.h"

@interface YKSError ()

@property (nonatomic, readwrite) YKSErrorCode errorCode;
@property (nonatomic, strong, readwrite) NSString * errorDescription;

@end

@implementation YKSError

+ (instancetype)newWithErrorCode:(YKSErrorCode)errorCode errorDescription:(NSString *)errorDescription
{
    return [[YKSError alloc] initWithErrorCode:errorCode errorDescription:errorDescription];
}

- (instancetype)initWithErrorCode:(YKSErrorCode)errorCode errorDescription:(NSString *)errorDescription
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.errorCode = errorCode;
    self.errorDescription = errorDescription;
    
    return self;
}

+ (instancetype)yikesErrorFromNSError:(NSError *)error
{
    // TODO: convert NSError to YKSError
    return nil;
}

@end
