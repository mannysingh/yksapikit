//
//  YKSUserInfo.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/16/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YKSUserInfo : NSObject

@property (nonatomic, strong, readonly) NSString * email;
@property (nonatomic, strong, readonly) NSString * firstName;
@property (nonatomic, strong, readonly) NSString * lastName;
@property (nonatomic, strong, readonly) NSString * phone;

+ (instancetype)newWithJSONDictionary:(NSDictionary *)dictionary;

@end
