//
//  YKSStayInfo.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/16/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKSUserInfo.h"

@interface YKSStayInfo : NSObject

@property (nonatomic, strong, readonly) NSString * hotelName;
@property (nonatomic, strong, readonly) NSString * hotelAddress;
@property (nonatomic, strong, readonly) NSString * roomNumber;
@property (nonatomic, strong, readonly) NSDate * arrivalDate;
@property (nonatomic, strong, readonly) NSString * checkInTime;
@property (nonatomic, strong, readonly) NSDate * departDate;
@property (nonatomic, strong, readonly) NSString * checkOutTime;

@property (nonatomic, strong, readonly) YKSUserInfo * user;

+ (instancetype)newWithJSONDictionary:(NSDictionary *)dictionary;

@end
