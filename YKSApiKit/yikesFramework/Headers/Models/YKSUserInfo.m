//
//  YKSUserInfo.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/16/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSUserInfo.h"

@interface YKSUserInfo ()

@property (nonatomic, strong, readwrite) NSString * email;
@property (nonatomic, strong, readwrite) NSString * firstName;
@property (nonatomic, strong, readwrite) NSString * lastName;
@property (nonatomic, strong, readwrite) NSString * phone;

@end

@implementation YKSUserInfo

+ (instancetype)newWithJSONDictionary:(NSDictionary *)dictionary
{
    return [[YKSUserInfo alloc] initWithJSONDictionary:dictionary];
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.email = dictionary[@"email"];
    self.firstName = dictionary[@"first_name"];
    self.lastName = dictionary[@"last_name"];
    self.phone = dictionary[@"phone_number"];
    
    return self;
}

@end
