//
//  YKSStayInfo.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/16/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSStayInfo.h"

@interface YKSStayInfo ()

@property (nonatomic, strong, readwrite) NSString * hotelName;
@property (nonatomic, strong, readwrite) NSString * hotelAddress;
@property (nonatomic, strong, readwrite) NSString * roomNumber;
@property (nonatomic, strong, readwrite) NSDate * arrivalDate;
@property (nonatomic, strong, readwrite) NSString * checkInTime;
@property (nonatomic, strong, readwrite) NSDate * departDate;
@property (nonatomic, strong, readwrite) NSString * checkOutTime;

@property (nonatomic, strong, readwrite) YKSUserInfo * user;

@end

@implementation YKSStayInfo

+ (instancetype)newWithJSONDictionary:(NSDictionary *)dictionary
{
    return [[YKSStayInfo alloc] initWithJSONDictionary:dictionary];
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    self.hotelName = dictionary[@"hotel_name"];
    self.hotelAddress = dictionary[@"hotel_address"];
    self.roomNumber = dictionary[@"room_number"];
    self.arrivalDate = [dateFormat dateFromString:dictionary[@"arrival_date"]];
    self.checkInTime = dictionary[@"check_in_time"];
    self.departDate = [dateFormat dateFromString:dictionary[@"depart_date"]];
    self.checkOutTime = dictionary[@"check_out_time"];
    
    self.user = [YKSUserInfo newWithJSONDictionary:dictionary[@"user"]];
    
    return self;
}

@end
