//
//  YikesKit.h
//  YikesKit
//
//  Created by Manny Singh on 4/8/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSUserInfo.h"
#import "YKSStayInfo.h"
#import "YKSConstants.h"
#import "YKSError.h"

@interface YikesKit : NSObject

/**
 *  The level of logging detail for APIManager.
 *
 *  YKSLoggerLevelOff
 *      - Do not log anything from APIManager
 *
 *  YKSLoggerLevelDebug
 *      - Logs HTTP method, URL, header fields, & request body for requests, and status code, URL, header fields, response string, & elapsed time for responses.
 *
 *  YKSLoggerLevelInfo  (default)
 *      - Logs HTTP method & URL for requests, and status code, URL, & elapsed time for responses.
 *
 *  YKSLoggerLevelError
 *      - Logs HTTP method & URL for requests, and status code, URL, & elapsed time for responses, but only for failed requests.
 *
 */
@property (nonatomic, assign) YKSLoggerLevel loggingLevelForAPIManager;

/**
 *  The level of logging detail for BLEManager.
 *
 *  YKSLoggerLevelOff
 *      - Do not log anything from BLEManager
 *
 *  YKSLoggerLevelDebug
 *      - TODO: YKSLoggerLevelDebug
 *
 *  YKSLoggerLevelInfo  (default)
 *      - TODO: YKSLoggerLevelInfo
 *
 *  YKSLoggerLevelError
 *      - TODO: YKSLoggerLevelError
 *
 */
@property (nonatomic, assign) YKSLoggerLevel loggingLevelForBLEManager;

+ (instancetype)sharedKit;

/**
 *  Initial setup for APIManager & BLEManager.
 */
- (void)setupKit;
- (void)setupKitWithApiEnvironment:(YKSApiEnv)apiEnv;

+ (BOOL)isSessionActive;

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void(^)(YKSUserInfo *user))successBlock
                  failure:(void(^)(YKSError *error))failureBlock;

- (void)logoutWithSuccess:(void(^)())successBlock
                  failure:(void(^)(YKSError *error))failureBlock;

- (void)getUserWithSuccess:(void(^)(YKSUserInfo *user))successBlock
                   failure:(void(^)(YKSError *error))failureBlock;

- (void)getUserStaysWithSuccess:(void(^)(NSArray *stays))successBlock
                        failure:(void(^)(YKSError *error))failureBlock;

- (void)registerUserWithForm:(NSDictionary *)form
                     success:(void(^)(YKSUserInfo *user))successBlock
                     failure:(void(^)(YKSError *error))failureBlock;

@end
