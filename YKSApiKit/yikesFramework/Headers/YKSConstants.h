//
//  YKSConstants.h
//  YikesKit
//
//  Created by Manny Singh on 4/11/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#ifndef YKSApiKit_YKSConstants_h
#define YKSApiKit_YKSConstants_h

/**
 *  Api enviroment types.
 */
typedef NS_ENUM(NSUInteger, YKSApiEnv) {
    kYKSEnvPROD,
    kYKSEnvQA,
    kYKSEnvDEV
};

/**
 *  LoggerLevel for APIManager & BLEManager, see @YikesKit for info.
 */
typedef NS_ENUM(NSUInteger, YKSLoggerLevel) {
    kYKSLoggerLevelOff,
    kYKSLoggerLevelDebug,
    kYKSLoggerLevelInfo,
    kYKSLoggerLevelError
};

/**
 *  Device services that are required by YikesKit.
 */
typedef NS_ENUM(NSUInteger, YKSServiceType) {
    kYKSInternetConnectionService,
    kYKSBluetoothService,
    kYKSLocationService,
    kYKSPushNotificationService
};

/**
 *  Current event state for GuestApp w/ BLE triangle.
 */
typedef NS_ENUM(NSUInteger, YKSEventType) {
    kYKSEventEnteredHotel,
    kYKSEventArrivedOnFloor,
    kYKSEventConnectingToDoor,
    kYKSEventConnectionRetrying,
    kYKSEventConnectedToDoor,
    kYKSEventDisconnectedFromDoor,
    kYKSEventLeftHotel
};

/**
 *  Errors for API & BLE.
 */
typedef NS_ENUM(NSUInteger, YKSErrorCode) {
    kYKSErrorUnknown,
    kYKSErrorMissingRequiredParameters,
    kYKSErrorUserEmailAlreadyRegistered
};

#endif
