//
//  YKSError.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/16/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKSConstants.h"

@interface YKSError : NSObject

@property (nonatomic, readonly) YKSErrorCode errorCode;
@property (nonatomic, strong, readonly) NSString * errorDescription;

+ (instancetype)newWithErrorCode:(YKSErrorCode)errorCode errorDescription:(NSString *)errorDescription;
+ (instancetype)yikesErrorFromNSError:(NSError*)error;

@end
