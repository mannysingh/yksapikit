//
//  YikesKit.m
//  YikesKit
//
//  Created by Manny Singh on 4/8/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YikesKit.h"
#import "YKSAPIManager.h"
#import "YKSSessionManager.h"

@implementation YikesKit

+ (instancetype)sharedKit
{
    static YikesKit *_sharedKit = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedKit = [[YikesKit alloc] init];
    });
    
    return _sharedKit;
}

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    /* Default log level */
    _loggingLevelForAPIManager = kYKSLoggerLevelInfo;
    _loggingLevelForBLEManager = kYKSLoggerLevelInfo;
    
    return self;
}

- (void)setupKit
{
    [self setupKitWithApiEnvironment:kYKSEnvPROD];
}

- (void)setupKitWithApiEnvironment:(YKSApiEnv)apiEnv
{
    // init APIManager & BLEManager here
    [YKSAPIManager setupAPIManagerWithLogLevel:self.loggingLevelForAPIManager apiEnv:apiEnv];
    //[YKSBLEManager setupBLEManagerWithLogLevel:self.loggingLevelForBLEManager];
}

- (void)setLoggingLevelForAPIManager:(YKSLoggerLevel)level
{
    _loggingLevelForAPIManager = level;
    [[YKSAPIManager sharedManager] setLoggingLevelForAPIManager:_loggingLevelForAPIManager];
}

- (void)setLoggingLevelForBLEManager:(YKSLoggerLevel)level
{
    _loggingLevelForBLEManager = level;
    //[[YKSBLEManager sharedManager] setLoggingLevelForBLEManager:_loggingLevelForBLEManager];
}

+ (BOOL)isSessionActive
{
    return [YKSSessionManager isSessionActive];
}

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void (^)(YKSUserInfo *))successBlock
                  failure:(void (^)(YKSError *))failureBlock
{
    if (!username || !password) {
        failureBlock([YKSError newWithErrorCode:kYKSErrorMissingRequiredParameters errorDescription:@""]);
    }
    
    [YKSAPIManager loginWithUsername:username password:password success:^(YKSUser *user) {
        
        NSError *error = nil;
        NSDictionary *userJSON = [MTLJSONAdapter JSONDictionaryFromModel:user error:&error];
        if (!error) {
            YKSUserInfo *userInfo = [YKSUserInfo newWithJSONDictionary:userJSON];
            successBlock(userInfo);
        } else {
            failureBlock(nil);
        }
        
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

- (void)logoutWithSuccess:(void (^)())successBlock failure:(void (^)(YKSError *))failureBlock
{
    [YKSAPIManager logoutWithSuccess:^{
        successBlock();
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

- (void)getUserWithSuccess:(void (^)(YKSUserInfo *))successBlock
                   failure:(void (^)(YKSError *))failureBlock
{
    [YKSAPIManager getCurrentUserWithSuccess:^(YKSUser *user) {
        
        NSError *error = nil;
        NSDictionary *userJSON = [MTLJSONAdapter JSONDictionaryFromModel:user error:&error];
        if (!error) {
            YKSUserInfo *userInfo = [YKSUserInfo newWithJSONDictionary:userJSON];
            successBlock(userInfo);
        } else {
            failureBlock(nil);
        }
        
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

- (void)getUserStaysWithSuccess:(void (^)(NSArray *))successBlock
                        failure:(void (^)(YKSError *))failureBlock
{
    [YKSAPIManager getCurrentUserStaysWithSuccess:^(NSArray *stays) {
        
        NSMutableArray *listOfStayInfos = [NSMutableArray new];
        
        [stays enumerateObjectsUsingBlock:^(YKSStay *stay, NSUInteger idx, BOOL *stop) {
            
            NSError *error = nil;
            NSDictionary *stayJSON = [MTLJSONAdapter JSONDictionaryFromModel:stay error:&error];
            if (!error) {
                YKSStayInfo *stayInfo = [YKSStayInfo newWithJSONDictionary:stayJSON];
                [listOfStayInfos addObject:stayInfo];
            }
            
        }];
        
        successBlock(listOfStayInfos);
        
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

- (void)registerUserWithForm:(NSDictionary *)form
                     success:(void (^)(YKSUserInfo *))successBlock
                     failure:(void (^)(YKSError *))failureBlock
{
    if (!form || !form[@"email"]) {
        failureBlock([YKSError newWithErrorCode:kYKSErrorMissingRequiredParameters errorDescription:@""]);
    }
    
    [YKSAPIManager checkIfEmailIsRegistered:form[@"email"] success:^(BOOL isAlreadyRegistered) {
        
        if (!isAlreadyRegistered) {
            [YKSAPIManager registerUserWithForm:form success:^(YKSUser *user) {
                
                NSError *error = nil;
                NSDictionary *userJSON = [MTLJSONAdapter JSONDictionaryFromModel:user error:&error];
                if (!error) {
                    YKSUserInfo *userInfo = [YKSUserInfo newWithJSONDictionary:userJSON];
                    successBlock(userInfo);
                } else {
                    failureBlock(nil);
                }
                
            } failure:^(NSError *error) {
                failureBlock(nil);
            }];
        } else {
            failureBlock([YKSError newWithErrorCode:kYKSErrorUserEmailAlreadyRegistered errorDescription:@""]);
        }
        
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
    
}


@end
