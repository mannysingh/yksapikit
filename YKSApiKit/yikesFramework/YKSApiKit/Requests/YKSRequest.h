//
//  YKSRequest.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

/**
 *  Base Request class
 */

#import "YKSHTTPClient.h"

@interface YKSRequest : NSObject


/**
 *  API Endpoints
 */
+ (NSString *)apiURLForLogin;
+ (NSString *)apiURLForLogout;
+ (NSString *)apiURLForUsers;
+ (NSString *)apiURLForUserWithId:(NSNumber *)userId;
+ (NSString *)apiURLForStaysWithUserId:(NSNumber *)userId;
+ (NSString *)apiURLForHotelWithId:(NSNumber *)hotelId;
+ (NSString *)apiURLForVerifyWithEmail:(NSString *)email;

@end
