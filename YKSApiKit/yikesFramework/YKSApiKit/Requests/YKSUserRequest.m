//
//  YKSUserRequest.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSUserRequest.h"
#import "YKSUser.h"
#import "YKSStay.h"

@implementation YKSUserRequest

+ (void)getUserWithId:(NSNumber *)userId
              success:(void (^)(YKSUser *, AFHTTPRequestOperation *))successBlock
              failure:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    NSString *url = [self apiURLForUserWithId:userId];
    
    [[YKSHTTPClient operationManager] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *userJSON = responseObject[@"response_body"][@"user"];
        
        NSError *error = nil;
        YKSUser *user = [YKSUser newUserFromJSON:userJSON error:&error];
        
        if (!error) {
            successBlock(user, operation);
        } else {
            failureBlock(operation, error);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failureBlock(operation, error);
        
    }];
}

+ (void)getStaysWithUserId:(NSNumber *)userId
                   success:(void (^)(NSArray *, AFHTTPRequestOperation *))successBlock
                   failure:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    NSString *url = [self apiURLForStaysWithUserId:userId];
    
    [[YKSHTTPClient operationManager] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *staysArray = responseObject[@"response_body"][@"stay"];
        
        NSError *error = nil;
        NSArray *stays = [YKSStay newStaysFromJSON:staysArray error:&error];
        
        if (!error) {
            successBlock(stays, operation);
        } else {
            failureBlock(operation, error);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        failureBlock(operation, error);
        
    }];
}

@end
