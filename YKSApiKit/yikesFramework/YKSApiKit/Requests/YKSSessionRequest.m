//
//  YKSSessionRequest.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSSessionRequest.h"
#import "YKSUser.h"

@implementation YKSSessionRequest

+ (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void (^)(YKSUser *, AFHTTPRequestOperation *))successBlock
                  failure:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    NSString *url = [self apiURLForLogin];
    NSDictionary *params = @{
                             @"user_name":username,
                             @"password":password,
                             @"_expand": @"user"
                             };
    
    [[YKSHTTPClient operationManager] POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *userJSON = responseObject[@"response_body"][@"user"];
        
        NSError *error = nil;
        YKSUser *user = [YKSUser newUserFromJSON:userJSON error:&error];
        
        if (!error) {
            successBlock(user, operation);
        } else {
            failureBlock(operation, error);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failureBlock(operation, error);
        
    }];
    
}

+ (void)logoutWithSuccess:(void (^)(AFHTTPRequestOperation *))successBlock
                  failure:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    NSString *url = [self apiURLForLogout];
    [[YKSHTTPClient operationManager] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock(operation);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(operation, error);
    }];
}

+ (void)checkIfEmailIsRegistered:(NSString *)email
                         success:(void (^)(BOOL, AFHTTPRequestOperation *))successBlock
                         failure:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    NSString *url = [self apiURLForVerifyWithEmail:email];
    [[YKSHTTPClient operationManager] GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (operation.response.statusCode == 200) {
            
            /* Email already registered */
            successBlock(YES, operation);
        } else {
            
            /* Email not registered yet */
            successBlock(NO, operation);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(operation, error);
    }];
}

// TODO: finish this method
+ (void)registerUserWithForm:(NSDictionary *)form
                     success:(void (^)(YKSUser *, AFHTTPRequestOperation *))successBlock
                     failure:(void (^)(AFHTTPRequestOperation *, NSError *))failureBlock
{
    NSString *url = [self apiURLForUsers];
    [[YKSHTTPClient operationManager] POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // INCOMPLETE
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(operation, error);
    }];
}

@end
