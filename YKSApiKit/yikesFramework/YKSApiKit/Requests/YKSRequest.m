//
//  YKSRequest.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSRequest.h"

@implementation YKSRequest

+ (NSString *)apiURLForLogin
{
    return @"/ycentral/api/session/login";
}

+ (NSString *)apiURLForLogout
{
    return @"/ycentral/api/session/logout";
}

+ (NSString *)apiURLForUsers
{
    return @"/ycentral/api/users";
}

+ (NSString *)apiURLForUserWithId:(NSNumber *)userId
{
    return [NSString stringWithFormat:@"/ycentral/api/users/%li", (long)userId.integerValue];
}

+ (NSString *)apiURLForStaysWithUserId:(NSNumber *)userId
{
    return [NSString stringWithFormat:@"/ycentral/api/users/%li/stays", (long)userId.integerValue];
}

+ (NSString *)apiURLForHotelWithId:(NSNumber *)hotelId
{
    return [NSString stringWithFormat:@"/ycentral/api/hotels/%li", (long)hotelId.integerValue];
}

+ (NSString *)apiURLForVerifyWithEmail:(NSString *)email
{
    return [NSString stringWithFormat:@"/ycentral/api/verify?email=%@", email];
}

@end
