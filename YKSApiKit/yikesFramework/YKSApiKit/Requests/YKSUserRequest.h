//
//  YKSUserRequest.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

/**
 *  Request class that includes API calls involving a user.
 */

#import "YKSRequest.h"

@class YKSUser;

@interface YKSUserRequest : YKSRequest

+ (void)getUserWithId:(NSNumber *)userId
              success:(void(^)(YKSUser *user, AFHTTPRequestOperation *operation))successBlock
              failure:(void(^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

+ (void)getStaysWithUserId:(NSNumber *)userId
                   success:(void(^)(NSArray *stays, AFHTTPRequestOperation *operation))successBlock
                   failure:(void(^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

@end
