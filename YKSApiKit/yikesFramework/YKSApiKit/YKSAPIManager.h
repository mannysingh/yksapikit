//
//  YKSAPIManager.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

/**
 *  Master APIManager class
 *  Includes all api call methods that will be used in either yikes GA, yikes HA, or third-party app.
 */

#import "YikesKit.h"
#import "YKSSession.h"
#import "YKSUser.h"
#import "YKSHotel.h"
#import "YKSStay.h"
#import "YKSPrimaryYMan.h"
#import "YKSWeather.h"
#import "YKSAddress.h"
#import "YKSInteralConstants.h"

@interface YKSAPIManager : NSObject

@property (nonatomic, assign) YKSLoggerLevel loggingLevelForAPIManager;

+ (instancetype)sharedManager;

+ (void)setupAPIManagerWithLogLevel:(YKSLoggerLevel)level apiEnv:(YKSApiEnv)apiEnv appName:(YKSAppName)appName;
+ (void)setupAPIManagerWithLogLevel:(YKSLoggerLevel)level apiEnv:(YKSApiEnv)apiEnv;

+ (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void(^)(YKSUser *user))successBlock
                  failure:(void(^)(NSError *error))failureBlock;

+ (void)logoutWithSuccess:(void(^)())successBlock
                  failure:(void(^)(NSError *error))failureBlock;

+ (void)getCurrentUserWithSuccess:(void(^)(YKSUser *user))successBlock
                          failure:(void(^)(NSError *error))failureBlock;

+ (void)getCurrentUserStaysWithSuccess:(void(^)(NSArray *stays))successBlock
                               failure:(void(^)(NSError *error))failureBlock;

+ (void)getHotelWithId:(NSNumber *)hotelId
               success:(void(^)(YKSHotel *hotel))successBlock
               failure:(void(^)(NSError *error))failureBlock;

+ (void)checkIfEmailIsRegistered:(NSString *)email
                         success:(void(^)(BOOL isAlreadyRegistered))successBlock
                         failure:(void(^)(NSError *error))failureBlock;

+ (void)registerUserWithForm:(NSDictionary *)form
                     success:(void(^)(YKSUser *user))successBlock
                     failure:(void(^)(NSError *error))failureBlock;


@end
