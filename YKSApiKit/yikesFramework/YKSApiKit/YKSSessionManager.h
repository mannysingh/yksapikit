//
//  YKSSessionManager.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/5/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

/**
 *  Manages session states.
 *  Responsible for create, restoring, & destroying session.
 */

@class YKSUser;

@interface YKSSessionManager : NSObject

+ (instancetype)sharedManager;

/**
 *  Create a new session for @param user
 */
+ (void)newSessionWithUser:(YKSUser *)user;

/**
 *  Save @param stays current user session
 */
+ (void)setCurrentUserStays:(NSArray *)stays;

/**
 *  Check to see if session cookie exists.
 */
+ (BOOL)isSessionActive;

/**
 *  Restore cookie from saved session
 */
+ (void)restoreSessionFromCache;

/**
 *  Clear session and delete session cookie
 */
+ (void)destroySession;

/**
 *  Returns the session cookie
 */
+ (NSHTTPCookie *)getSessionCookie;

@end
