//
//  YKSHotel.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSHotel.h"
#import "YKSWeather.h"
#import "YKSAddress.h"

@implementation YKSHotel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"checkInTime": @"check_in_time",
             @"checkOutTime": @"check_out_time",
             @"contactPhone": @"contact_phone",
             @"id": @"id",
             @"name": @"name",
             @"address": @"address"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    // Store a value that needs to be determined locally upon initialization.
    
    return self;
}

+ (NSValueTransformer *)addressTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YKSAddress.class];
}

+ (NSArray *)newHotelsFromJSON:(NSArray *)JSONArray error:(NSError *__autoreleasing *)error
{
    return [MTLJSONAdapter modelsOfClass:YKSHotel.class fromJSONArray:JSONArray error:error];
}

+ (YKSHotel *)newHotelFromJSON:(NSDictionary *)JSONDictionary error:(NSError *__autoreleasing *)error
{
    return [MTLJSONAdapter modelOfClass:YKSHotel.class fromJSONDictionary:JSONDictionary error:error];
}

@end
