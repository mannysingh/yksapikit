//
//  YKSSession.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSSession.h"
#import "YKSUser.h"
#import "YKSSessionManager.h"

@implementation YKSSession

+ (instancetype)sharedSession
{
    static YKSSession *_sharedSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        YKSSession *cachedSession = [YKSSession sessionFromCache];
        if (cachedSession) {
            _sharedSession = cachedSession;
        } else {
            _sharedSession = [[YKSSession alloc] init];
        }
        
    });
    return _sharedSession;
}

+ (YKSSession *)sessionFromCache
{
    return [self rootObjectWithCacheName:NSStringFromClass([self class])];
}

+ (void)saveToCache
{
    [self saveWithRootObject:[YKSSession sharedSession] withCacheName:NSStringFromClass([self class])];
}

@end
