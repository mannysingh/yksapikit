//
//  YKSSession.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

/**
 *  Session model used to store currentUser, currentStays, & sessionCookie
 */

#import "YKSModel.h"

@class YKSUser;

@interface YKSSession : YKSModel

@property (nonatomic, strong) YKSUser *currentUser;
@property (nonatomic, strong) NSArray *currentStays;
@property (nonatomic, strong) NSHTTPCookie *sessionCookie;

/**
 *  If an instance exists in file, this will load singleton from file.
 *  If not, it will create a new empty session.
 */
+ (instancetype)sharedSession;

/**
 *  Saves YKSSession singleton to file.
 */
+ (void)saveToCache;

@end
