//
//  YKSStay.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSStay.h"
#import "YKSHotel.h"
#import "YKSUser.h"
#import "YKSPrimaryYMan.h"

@implementation YKSStay

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"arrivalDate": @"arrival_date",
             @"checkInTime": @"check_in_time",
             @"checkOutTime": @"check_out_time",
             @"createdOn": @"created_on",
             @"departDate": @"depart_date",
             @"hotelEquipmentId": @"hotel_equipment_id",
             @"hotelId": @"hotel_id",
             @"id": @"id",
             @"isActive": @"is_active",
             @"isAtHotel": @"is_at_hotel",
             @"isCancelled": @"is_cancelled",
             @"isDeparted": @"is_departed",
             @"modifiedOn": @"modified_on",
             @"reservationNumber": @"reservation_number",
             @"roomNumber": @"room_number",
             @"roomType": @"room_type",
             @"stayComment": @"stay_comment",
             @"userId": @"user_id",
             @"primaryYMen": @"primary_ymen"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    // Store a value that needs to be determined locally upon initialization.
    
    return self;
}

+ (NSValueTransformer *)primaryYMenTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YKSPrimaryYMan.class];
}

+ (NSArray *)newStaysFromJSON:(NSArray *)JSONArray error:(NSError *__autoreleasing *)error
{
    return [MTLJSONAdapter modelsOfClass:YKSStay.class fromJSONArray:JSONArray error:error];
}

+ (YKSStay *)newStayFromJSON:(NSDictionary *)JSONDictionary error:(NSError *__autoreleasing *)error
{
    return [MTLJSONAdapter modelOfClass:YKSStay.class fromJSONDictionary:JSONDictionary error:error];
}

@end
