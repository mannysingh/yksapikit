//
//  YKSUser.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSUser.h"

@implementation YKSUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"firstApiLoginOn": @"first_api_login_on",
             @"createdOn": @"created_on",
             @"deviceId": @"device_id",
             @"email": @"email",
             @"firstName": @"first_name",
             @"id": @"id",
             @"lastName": @"last_name",
             @"middleInitial": @"middle_initial",
             @"phoneNumber": @"phone_number",
             @"primaryPhone": @"primary_phone"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    // Store a value that needs to be determined locally upon initialization.
    
    return self;
}

+ (NSArray *)newUsersFromJSON:(NSArray *)JSONArray error:(NSError *__autoreleasing *)error
{
    return [MTLJSONAdapter modelsOfClass:YKSUser.class fromJSONArray:JSONArray error:error];
}

+ (YKSUser *)newUserFromJSON:(NSDictionary *)JSONDictionary error:(NSError *__autoreleasing *)error
{
    return [MTLJSONAdapter modelOfClass:YKSUser.class fromJSONDictionary:JSONDictionary error:error];
}

@end
