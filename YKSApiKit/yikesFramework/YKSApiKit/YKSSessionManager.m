//
//  YKSSessionManager.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/5/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSSessionManager.h"
#import "YKSHTTPClient.h"
#import "YKSUser.h"
#import "YKSSession.h"

@implementation YKSSessionManager

+ (instancetype)sharedManager
{
    static YKSSessionManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[YKSSessionManager alloc] init];
        [YKSSession sharedSession];
    });
    
    return _sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActiveNotification:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackgroundNotification:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    return self;
}

- (void)appDidBecomeActiveNotification:(NSNotification *)notification
{
    [YKSSessionManager restoreSessionFromCache];
}

- (void)appDidEnterBackgroundNotification:(NSNotification *)notification
{
    [YKSSession saveToCache];
}

+ (void)newSessionWithUser:(YKSUser *)user
{
    [YKSSession sharedSession].currentUser = user;
    [YKSSessionManager checkAndSetSessionCookie];
    [YKSSession saveToCache];
}

+ (void)destroySession
{
    [YKSSession sharedSession].currentUser = nil;
    [YKSSession sharedSession].sessionCookie = nil;
    [YKSSession sharedSession].currentStays = nil;
    [YKSSessionManager deleteSessionCookie];
    [YKSSession saveToCache];
}

+ (void)setCurrentUserStays:(NSArray *)stays
{
    [YKSSession sharedSession].currentStays = stays;
    [YKSSession saveToCache];
}

+ (BOOL)isSessionActive
{
    return [YKSSessionManager getSessionCookie] ? YES : NO;
}

+ (void)restoreSessionFromCache
{
    NSHTTPCookie *sessionCookie = [YKSSession sharedSession].sessionCookie;
    if (sessionCookie) {
        NSArray *cookieArray = [NSArray arrayWithObject:sessionCookie];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:cookieArray forURL:[YKSHTTPClient operationManager].baseURL mainDocumentURL:nil];
    }
}

+ (BOOL)checkAndSetSessionCookie
{
    NSHTTPCookie *sessionCookie = [self getSessionCookie];
    if (sessionCookie) {
        [YKSSession sharedSession].sessionCookie = [NSHTTPCookie cookieWithProperties:sessionCookie.properties];
        return YES;
    }
    return NO;
}

+ (NSHTTPCookie *)getSessionCookie
{
    NSURL *baseURL = [[YKSHTTPClient operationManager] baseURL];
    NSArray *allCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:baseURL];
    
    __block NSHTTPCookie *cookie = nil;
    [allCookies enumerateObjectsUsingBlock:^(NSHTTPCookie *cookieObj, NSUInteger idx, BOOL *stop) {
        if ([cookieObj.name isEqualToString:yksSessionCookieName]) {
            cookie = cookieObj;
            *stop = YES;
        }
    }];
    return cookie;
}

+ (void)deleteSessionCookie
{
    NSURL *baseURL = [[YKSHTTPClient operationManager] baseURL];
    NSArray *allCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:baseURL];
    
    [allCookies enumerateObjectsUsingBlock:^(NSHTTPCookie *cookieObj, NSUInteger idx, BOOL *stop) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookieObj];
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

// TODO: need an alternative way to extend session instead of saving guest password
//+ (BOOL)storeCurrentGuestAppUserEmail:(NSString *)email
//                          andPassword:(NSString *)password
//{
//    BOOL stored = YES;
//    if (![SSKeychain setPassword:email forService:yksKeychainGuestAppServiceName account:yksKeychainGuestEmailAccountName]) {
//        stored = NO;
//        DLog(@"User's email could not be stored in keychain");
//    }
//    if (![SSKeychain setPassword:password forService:yksKeychainGuestAppServiceName account:yksKeychainGuestPasswordAccountName]) {
//        stored = NO;
//        DLog(@"User's password could not be stored in keychain");
//    }
//    return stored;
//}

//+ (BOOL)storeGuestAppSessionToKeychain
//{
//    NSHTTPCookie *sessionCookie = [self getSessionCookie];
//    NSString *cookiePropertiesDesc = [NSString stringWithFormat:@"%@", sessionCookie.properties];
//    BOOL stored = YES;
//    if (![SSKeychain setPassword:cookiePropertiesDesc forService:yksKeychainGuestAppServiceName account:yksKeychainSessionTokenAccountName]) {
//        stored = NO;
//        DLog(@"User's session cookie could not be stored in keychain");
//    }
//    return stored;
//}
//
//+ (BOOL)storeHotelAppSessionToKeychain
//{
//    NSHTTPCookie *sessionCookie = [self getSessionCookie];
//    NSString *cookiePropertiesDesc = [NSString stringWithFormat:@"%@", sessionCookie.properties];
//    BOOL stored = YES;
//    if (![SSKeychain setPassword:cookiePropertiesDesc forService:yksKeychainHotelAppServiceName account:yksKeychainSessionTokenAccountName]) {
//        stored = NO;
//        DLog(@"User's session cookie could not be stored in keychain");
//    }
//    return stored;
//}

//+ (void)loadGuestAppSessionFromKeychain
//{
//    NSString *sessionToken = [SSKeychain passwordForService:yksKeychainGuestAppServiceName account:yksKeychainSessionTokenAccountName];
//}
//
//+ (void)loadHotelAppSessionFromKeychain
//{
//    NSString *sessionToken = [SSKeychain passwordForService:yksKeychainGuestAppServiceName account:yksKeychainSessionTokenAccountName];
//}

//+ (BOOL)storeCurrentHotelAppUserEmail:(NSString *)email
//                          andPassword:(NSString *)password
//{
//    BOOL stored = YES;
//    if (![SSKeychain setPassword:email forService:yksKeychainHotelAppServiceName account:yksKeychainHotelEmailAccountName]) {
//        stored = NO;
//        DLog(@"Hotel user's email could not be stored in keychain");
//    }
//    if (![SSKeychain setPassword:password forService:yksKeychainHotelAppServiceName account:yksKeychainHotelPasswordAccountName]) {
//        stored = NO;
//        DLog(@"Hotel user's password could not be stored in keychain");
//    }
//    return stored;
//}

@end
