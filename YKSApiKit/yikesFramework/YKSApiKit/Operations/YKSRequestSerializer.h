//
//  YKSRequestSerializer.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "AFURLRequestSerialization.h"

@interface YKSRequestSerializer : AFJSONRequestSerializer

@end
