//
//  YKSInteralConstants.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#ifndef YKSApiKit_YKSInteralConstants_h
#define YKSApiKit_YKSInteralConstants_h


#ifndef DEBUG
    /*  Don't log if not in debug mode  */
    #define DLog(...) do { } while (0)
#else
    /*  Pretty log if in debug mode  */
    #define DLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#endif


/**
 *  Used to determine if using GuestApp or HotelApp.
 */
typedef NS_ENUM(NSUInteger, YKSAppName) {
    yksNoAppName,
    yksGuestApp,
    yksHotelApp
};

/**
 *  Base URLs for api environments.
 */
static NSString *const yksDEVBaseURLString  = @"https://dev-api.yikes.co";
static NSString *const yksQABaseURLString   = @"https://qa-api.yikes.co";
static NSString *const yksPRODBaseURLString = @"https://api.yikes.co";

/**
 *  NSUserDefaults key used to store appName.
 */
static NSString *const yksAppNameKey = @"yksAppNameUserDefaultsKey";

/**
 *  NSUserDefaults key used to store selected api environment.
 */
static NSString *const yksCurrentApiEnvKey = @"yksCurrentApiEnvUserDefaultsKey";


/**
 *  Name of cookie set by yCentral.
 */
static NSString *const yksSessionCookieName = @"session_id_ycentral";

/**
 *  Name of folder where yikes model cache is stored.
 */
static NSString *const yksModelCacheDirectoryFolderName     = @"yikesCache";

/**
 *  Keychain name used to store guest email, password, session token.
 */
static NSString *const yksKeychainGuestAppServiceName       = @"co.yikes.yikes";
static NSString *const yksKeychainGuestEmailAccountName     = @"co.yikes.yikes.email";
static NSString *const yksKeychainGuestPasswordAccountName  = @"co.yikes.yikes.password";

static NSString *const yksKeychainHotelAppServiceName       = @"co.yikes.yikes-Hotel";
static NSString *const yksKeychainHotelEmailAccountName     = @"co.yikes.yikes-Hotel.email";
static NSString *const yksKeychainHotelPasswordAccountName  = @"co.yikes.yikes-Hotel.password";

static NSString *const yksKeychainSessionTokenAccountName   = @"co.yikes.session.token";


#endif
