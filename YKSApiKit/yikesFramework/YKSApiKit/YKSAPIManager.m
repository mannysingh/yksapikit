//
//  YKSAPIManager.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "YKSAPIManager.h"
#import "AFNetworkActivityLogger.h"
#import "YKSSessionRequest.h"
#import "YKSUserRequest.h"
#import "YKSHotelRequest.h"
#import "YKSSessionManager.h"

@implementation YKSAPIManager

+ (instancetype)sharedManager
{
    static YKSAPIManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[YKSAPIManager alloc] init];
    });
    
    return _sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    
    return self;
}

+ (void)setupAPIManagerWithLogLevel:(YKSLoggerLevel)level apiEnv:(YKSApiEnv)apiEnv appName:(YKSAppName)appName
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:appName] forKey:yksAppNameKey];
    [YKSAPIManager setupAPIManagerWithLogLevel:level apiEnv:apiEnv];
}

+ (void)setupAPIManagerWithLogLevel:(YKSLoggerLevel)level apiEnv:(YKSApiEnv)apiEnv
{
    [YKSAPIManager sharedManager].loggingLevelForAPIManager = level;
    
    /* Override default API environment */
    [YKSHTTPClient sharedClient].currentApiEnv = apiEnv;
    
    /* Needed to restore session from cache */
    [YKSSessionManager sharedManager];
}

- (void)setLoggingLevelForAPIManager:(YKSLoggerLevel)level
{
    _loggingLevelForAPIManager = level;
    switch (_loggingLevelForAPIManager) {
        case kYKSLoggerLevelOff:
            [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelOff];
            break;
            
        case kYKSLoggerLevelDebug:
            [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
            break;
            
        case kYKSLoggerLevelInfo:
            [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelInfo];
            break;
            
        case kYKSLoggerLevelError:
            [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelError];
            break;
            
        default:
            break;
    }
}

+ (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void (^)(YKSUser *))successBlock
                  failure:(void (^)(NSError *))failureBlock
{
    [YKSSessionRequest loginWithUsername:username password:password success:^(YKSUser *user, AFHTTPRequestOperation *operation) {
        
        /* Start a new session */
        [YKSSessionManager newSessionWithUser:user];
        
        successBlock(user);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
}

+ (void)logoutWithSuccess:(void (^)())successBlock
                  failure:(void (^)(NSError *))failureBlock
{
    [YKSSessionRequest logoutWithSuccess:^(AFHTTPRequestOperation *operation) {
        
        /* Destroy current session */
        [YKSSessionManager destroySession];
        
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        /* Destroy current session */
        [YKSSessionManager destroySession];
        
        failureBlock(error);
    }];
}

+ (void)getCurrentUserWithSuccess:(void (^)(YKSUser *))successBlock
                          failure:(void (^)(NSError *))failureBlock
{
    YKSUser *currentUser = [YKSSession sharedSession].currentUser;
    [YKSUserRequest getUserWithId:currentUser.id success:^(YKSUser *user, AFHTTPRequestOperation *operation) {
        successBlock(user);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
}

+ (void)getCurrentUserStaysWithSuccess:(void (^)(NSArray *))successBlock
                               failure:(void (^)(NSError *))failureBlock
{
    YKSUser *user = [YKSSession sharedSession].currentUser;
    
    [YKSUserRequest getStaysWithUserId:user.id success:^(NSArray *stays, AFHTTPRequestOperation *operation) {
        [YKSSessionManager setCurrentUserStays:stays];
        successBlock(stays);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
}

+ (void)getHotelWithId:(NSNumber *)hotelId
               success:(void (^)(YKSHotel *))successBlock
               failure:(void (^)(NSError *))failureBlock
{
    [YKSHotelRequest getHotelWithId:hotelId success:^(YKSHotel *hotel, AFHTTPRequestOperation *operation) {
        successBlock(hotel);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
}

+ (void)checkIfEmailIsRegistered:(NSString *)email
                         success:(void (^)(BOOL))successBlock
                         failure:(void (^)(NSError *))failureBlock
{
    [YKSSessionRequest checkIfEmailIsRegistered:email success:^(BOOL isAlreadyRegistered, AFHTTPRequestOperation *operation) {
        successBlock(isAlreadyRegistered);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
}

+ (void)registerUserWithForm:(NSDictionary *)form
                     success:(void (^)(YKSUser *))successBlock
                     failure:(void (^)(NSError *))failureBlock
{
    [YKSSessionRequest registerUserWithForm:form success:^(YKSUser *user, AFHTTPRequestOperation *operation) {
        successBlock(user);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureBlock(error);
    }];
}


@end
