//
//  ViewController.m
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import "ViewController.h"
#import "YikesKit.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *statusLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)onGetStaysBtnClick:(id)sender
{
    if (![YikesKit isSessionActive]) {
        
        [[YikesKit sharedKit] loginWithUsername:@"manny.singh@yikes.co" password:@"qwe123" success:^(YKSUserInfo *user) {
            
            [[YikesKit sharedKit] getUserWithSuccess:^(YKSUserInfo *user) {
                
                [[YikesKit sharedKit] getUserStaysWithSuccess:^(NSArray *stays) {
                    
                    self.statusLabel.text = @"GOT STAYS AFTER LOGIN!";
                    
                } failure:^(YKSError *error) {
                   
                    self.statusLabel.text = @"FAILED TO GET STAYSINFO LIST AFTER LOGIN";
                    
                }];
                
            } failure:^(YKSError *error) {
                
                self.statusLabel.text = @"FAILED TO GET USERINFO AFTER LOGIN";
                
            }];
            
        } failure:^(YKSError *error) {
            
            self.statusLabel.text = @"FAILED TO LOGIN";
            
        }];
        
    } else {
        
        [[YikesKit sharedKit] getUserWithSuccess:^(YKSUserInfo *user) {
            
            [[YikesKit sharedKit] getUserStaysWithSuccess:^(NSArray *stays) {
                
                self.statusLabel.text = @"GOT STAYS!";
                
            } failure:^(YKSError *error) {
                
                self.statusLabel.text = @"FAILED TO GET STAYSINFO LIST";
                
            }];
            
        } failure:^(YKSError *error) {
            
            self.statusLabel.text = @"FAILED TO GET USERINFO";
            
        }];
        
    }
}

- (IBAction)onLogoutBtnClick:(id)sender
{
    [[YikesKit sharedKit] logoutWithSuccess:^{
        
        self.statusLabel.text = @"LOGOUT SUCCESS";
        
    } failure:^(YKSError *error) {
       
        self.statusLabel.text = @"LOGOUT FAILED";
        
    }];
}

- (IBAction)onChangeEnvBtnClick:(id)sender
{
    [[YikesKit sharedKit] setupKitWithApiEnvironment:kYKSEnvDEV];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
