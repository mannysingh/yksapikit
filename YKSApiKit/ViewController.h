//
//  ViewController.h
//  YKSApiKit
//
//  Created by Manny Singh on 4/4/15.
//  Copyright (c) 2015 yikes. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef DEBUG
// Don't log if not in debug mode
#define DLog(...) do { } while (0)
#else
// Pretty log if in debug mode
#define DLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#endif

@interface ViewController : UIViewController


@end

